from django.shortcuts import render, redirect
from .forms import KegiatanForm, PesertaForm
from .models import Kegiatan, Peserta
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required(login_url='story9:login')
def activity(request):
    datas = {
        'kegiatan': Kegiatan.objects.all(),
        'peserta': Peserta.objects.all(),
    }
    data_forms = {
        'kegiatan_form': KegiatanForm(),
        'peserta_form': PesertaForm(),
    }
    response = {
        'forms': data_forms,
        'data': datas
    }
    return render(request, 'story6/activity.html', response)

@login_required(login_url='story9:login')
def post_kegiatan(request):
    if request.method == 'POST':
        form = KegiatanForm(request.POST or None)
        if form.is_valid():
            data_form = form.cleaned_data
            data = Kegiatan(nama=data_form['nama_kegiatan'])
            data.save()
            return redirect('story6:activity')
        else:
            return redirect('story6:activity')
    else:
        return redirect('story6:activity')

@login_required(login_url='story9:login')
def post_peserta(request, id_kegiatan):
    if request.method == 'POST':
        form = PesertaForm(request.POST or None)
        if form.is_valid():
            kegiatan = Kegiatan.objects.get(id=id_kegiatan)
            data_form = form.cleaned_data
            data_input = Peserta()
            data_input.nama = data_form['nama_peserta']
            data_input.kegiatan = kegiatan

            data_input.save()
            return redirect('story6:activity')
        else:
            return redirect('story6:activity')
    else:
        return redirect('story6:activity')
