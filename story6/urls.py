from django.urls import path
from . import views

app_name = 'story6'

urlpatterns = [
    path('post_kegiatan', views.post_kegiatan, name='post_kegiatan'),
    path('post_peserta/<int:id_kegiatan>', views.post_peserta, name='post_peserta'),
    path('', views.activity, name='activity'),
]