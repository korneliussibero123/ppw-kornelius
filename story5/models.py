from django.db import models

# Create your models here.

class Story5Model(models.Model):
    nama_matkul = models.CharField(max_length = 30)
    waktu = models.CharField(max_length=30)
    ruangan = models.CharField(max_length=30)

    def __str__(self):
        return "{}.{}".format(self.id, self.nama_matkul)
