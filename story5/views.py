from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import Story5ModelForm
from .models import Story5Model
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required(login_url='story9:login')
def create(request):
    form = Story5ModelForm(request.POST or None)

    if request.method == "POST":
        if form.is_valid():
            form.save()
        return redirect('story5:table')
    context = {
        'form' : form
    }
    return render(request, 'story5/create.html',context)

@login_required(login_url='story9:login')
def table(request):
    # if request.method == "POST":
    #     Story5Model.objects.create(nama_matkul = request.POST["nama_matkul "], waktu = request.POST["waktu "], ruangan = request.POST["ruangan"])
    daftar_form = Story5Model.objects.all()

    context = {
        'daftar_form' : daftar_form
    }

    return render(request, 'story5/table.html', context)


@login_required(login_url='story9:login')
def delete_matkul(request, id):
    Story5Model.objects.filter(pk=id).delete()
    return redirect('story5:table')
