from django import forms
from .models import Story5Model
from django.forms import ModelForm

class Story5ModelForm(forms.ModelForm):
    class Meta :
        model = Story5Model
        fields = [
            'nama_matkul',
            'waktu',
            'ruangan',    
        ]

        widgets = {
            'nama_matkul':forms.TextInput(attrs={'class':'form-control','placeholder':'Masukan nama mata kuliah'}),
            'waktu':forms.TextInput(attrs={'class':'form-control','placeholder':'Masukan waktu kuliah'}),
            'ruangan':forms.TextInput(attrs={'class':'form-control','placeholder':'Masukan nama ruangan'}),
        }
