from django.urls import path
from . import views

app_name = 'story5'

urlpatterns = [
    path('delete/<int:id>', views.delete_matkul, name='delete_matkul'),
    path('story5/', views.table, name='table'),
    path('', views.create, name='create')
]