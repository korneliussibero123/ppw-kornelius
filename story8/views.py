import requests
import json
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.http import JsonResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required


# Create your views here.

@login_required(login_url='story9:login')
def story8(request):
    response = {}
    return render(request, 'story8/story8.html', response)

@login_required(login_url='story9:login')
def data(request):
    arg = request.GET['q']
    url_terkait = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    r = requests.get(url_terkait)
    data = json.loads(r.content)
    return JsonResponse(data, safe=False)

