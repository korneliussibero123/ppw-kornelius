from django.test import TestCase
from django.test import Client
from django.urls import resolve

from . import views

class UnitTestForStory8(TestCase):

    # def test_response_page(self):
    #     response = Client().get('/story8/')
    #     self.assertEqual(response.status_code, 200)

    # def test_template_used(self):
    #     response = Client().get('/story8/')
    #     self.assertTemplateUsed(response, 'story8/story8.html')

    def test_func_page(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, views.story8)

    def test_func_page2(self):
        found = resolve('/data/')
        self.assertEqual(found.func, views.data)

    # def test_kelengkapan_formulir_di_view(self):
    #     response = Client().get('/story8/')
    #     isi_html_kembalian = response.content.decode('utf8')  
    #     self.assertIn("Daftar Buku", isi_html_kembalian)

    # def test_pemanggilan_tautan(self):
    #     response = Client().get('/data/?q=frozen%202')
    #     self.assertEqual(response.status_code, 200)

