from django.urls import path
from . import views

app_name = 'story3'

urlpatterns = [
    path('', views.project, name='project'),
]