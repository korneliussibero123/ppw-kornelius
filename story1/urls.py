from django.urls import path
from . import views

app_name = 'story1'

urlpatterns = [
    path('', views.index, name='index'),
    path('cv/', views.cv, name='cv'),
    path('index/', views.index, name='index')
]