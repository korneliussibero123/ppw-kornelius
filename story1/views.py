from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.

def index(request):
    return render(request, 'story1/index.html')

def cv(request):
    return render(request, 'story1/cv.html')