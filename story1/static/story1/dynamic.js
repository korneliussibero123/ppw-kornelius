// story7 area --------------------------------------------------------------------------------------------------
$(document).ready(function($){
    $('.accordion_box:first').addClass('activate')
    // $('.accordion_box:first').children('.acc_trigger').children('i').addClass('fa-chevron-down')
    // $('.accordion_box:first').children('.acc_trigger').children('i').addClass('fa-chevron-down')
    $('.accordion_box:first').children('.acc_trigger').addClass('selected').next('.acc_container').show()

    $('.acc_trigger').click(function(event){
        if($(this).hasClass('selected')){
            $(this).removeClass('selected');
            $(this).children('i').removeClass('fa-chevron-down');
            $(this).next().slideUp();
            $(this).parent().removeClass('activate');
        }else{
            $('.acc_trigger').removeClass('selected');
            $(this).addClass('selected');
            $('.acc_trigger').children('i').removeClass('fa-chevron-down');
            $(this).children('i').addClass('fa-chevron-down');
            $('.acc_trigger').next().slideUp();
            $(this).next().slideDown();
            $('.accordion_box').removeClass('active');
            $(this).parent().removeClass('active');

        }
        
    });
});

$(".reorder-up").click(function(){
    var $current = $(this).closest('.wrapper')
    var $previous = $current.prev('.wrapper');
    if($previous.length !== 0){
      $current.insertBefore($previous);
    }
    return false;
  });

  $(".reorder-down").click(function(){
    var $current = $(this).closest('.wrapper')
    var $next = $current.next('.wrapper');
    if($next.length !== 0){
      $current.insertAfter($next);
    }
    return false;
  });

// story8 area -----------------------------------------------------------------

$("#seo").keyup( function(){
  var seo_konten = $("#seo").val();
  var url_terkait = '/data?q=' + seo_konten;
  $.ajax({
    url: url_terkait,
    success: function(hasil){
      var obj_hasil = $("#tampilan");
      obj_hasil.empty();

      for(i = 0; i<hasil.items.length; i++){
        var title = hasil.items[i].volumeInfo.title;
        var thumbnails = hasil.items[i].volumeInfo.imageLinks.smallThumbnail;
        var authors = hasil.items[i].volumeInfo.authors;
        var publish = hasil.items[i].volumeInfo.publishedDate;
        var link = hasil.items[i].volumeInfo.previewLink;
        console.log(title);
        // obj_hasil.append('<div class="col-md-4">' + '<img src='+ thumbnails + '</div>');
        obj_hasil.append('<div class="row no-gutters">'+
        '<div class="col-md-4">'+
          '<img src='+thumbnails+ 'class="card-img" alt="webkornel">'+
        '</div>'+
        '<div class="col-md-8">'+
          '<div class="card-body">'+
            '<a target="_blank" href='+link+'>'+'<h5 class="card-title">' + title +'</h5> </a>'+
            '<p class="card-text">' + authors +'</p>'+
            '<p class="card-text"><small class="text-muted">' + publish +'</small></p>'+
          '</div>'+
        '</div>'+
      '</div>');
 
      }
    }
  });
});

