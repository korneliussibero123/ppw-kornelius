from django.test import TestCase
from django.test import Client
from django.urls import resolve

from . import views

class UnitTestForStory7(TestCase):

    # def test_response_page(self):
    #     response = Client().get('/story7/')
    #     self.assertEqual(response.status_code, 200)

    # def test_template_used(self):
    #     response = Client().get('/story7/')
    #     self.assertTemplateUsed(response, 'story7/story7.html')

    def test_func_page(self):
        found = resolve('/story7/')
        self.assertEqual(found.func, views.story7)

    # def test_kelengkapan_formulir_di_view(self):
    #     response = Client().get('/story7/')
    #     isi_html_kembalian = response.content.decode('utf8')  
    #     self.assertIn("Aktivitas", isi_html_kembalian)
    #     self.assertIn("Pengalaman", isi_html_kembalian)
    #     self.assertIn("Prestasi", isi_html_kembalian)
    #     self.assertIn("Project", isi_html_kembalian)