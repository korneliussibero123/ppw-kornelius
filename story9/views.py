from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.forms import UserCreationForm

from django.http import HttpResponse
from django.forms import inlineformset_factory
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.decorators import login_required

# Create your views here.

from .models import *
from .forms import CreateUserForm

def loginPage(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            request.session['username'] = request.user.username
            return redirect('story1:index')
        else:
            messages.info(request, 'Username OR password is incorrect')

    response = {}
    return render(request, 'story9/login.html', response)

@login_required(login_url='story9:login')
def logoutUser(request):
    logout(request)
    return redirect('/')

def registerPage(request):
    form = CreateUserForm()

    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')
            messages.success(request, 'Account was created for ' + user)

            return redirect('story9:login')

    response = {'form':form}
    return render(request, 'story9/register.html', response)

def story9(request):
    response = {}
    return render(request, 'story1/index.html', response)