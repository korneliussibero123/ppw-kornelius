from django.urls import path
from . import views

app_name = 'story9'

urlpatterns = [
    path('login/', views.loginPage, name='login'),
    path('register/', views.registerPage, name='register'),
    path('logout/', views.logoutUser, name="logout"),
    path('', views.story9, name='story9'),
]