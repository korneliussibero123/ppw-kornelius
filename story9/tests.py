from django.test import TestCase
from django.test import Client
from django.urls import resolve

from . import views

class UnitTestForStory9(TestCase):

    def test_response_page(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_template_used_1(self):
        response = Client().get('/story9/login/')
        self.assertTemplateUsed(response, 'story9/login.html')

    def test_template_used_2(self):
        response = Client().get('/story9/register/')
        self.assertTemplateUsed(response, 'story9/register.html')

    def test_func_login(self):
        found = resolve('/story9/login/')
        self.assertEqual(found.func, views.loginPage)

    def test_func_register(self):
        found = resolve('/story9/register/')
        self.assertEqual(found.func, views.registerPage)

    def test_func_logout(self):
        found = resolve('/story9/logout/')
        self.assertEqual(found.func, views.logoutUser)

    def test_func_story9(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, views.story9)

    def test_kelengkapan_formulir_di_login(self):
        response = Client().get('/story9/login/')
        isi_html_kembalian = response.content.decode('utf8')  
        self.assertIn("LOGIN", isi_html_kembalian)
        self.assertIn("Username..", isi_html_kembalian)
        self.assertIn("Password..", isi_html_kembalian)

    def test_kelengkapan_formulir_di_register(self):
        response = Client().get('/story9/register/')
        isi_html_kembalian = response.content.decode('utf8')  
        self.assertIn("REGISTER ACCOUNT", isi_html_kembalian)
        


